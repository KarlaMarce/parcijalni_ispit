<?php

function autoLoad($class)
{
    include dirname(__DIR__).'/'.$class.'.class.php';
}

spl_autoload_register(function($class){

    $file = dirname(__DIR__).'/'.$class.'.class.php';
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
    $file = strtolower($file);
    
    include $file;

});