<?php

namespace Controller;

class Base
{
    protected $db;

    public function __construct()
    {
        $connection = new \Lib\PdoMySql();
        $this->db = $connection->getConn();
    }

    protected function select($sql, $param = array())
    {
        $result = $this->db->prepare($sql);
        $result->execute($param);
        
        return $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    protected function select2($sql, $param = array())
    {
        $result = $this->db->prepare($sql);
        $result->execute($param);
        
        return $result->fetch(\PDO::FETCH_ASSOC);
    }

    protected function select3($sql, $param = array())
    {
        $result = $this->db->prepare($sql);
        $result->execute($param);
        
        return $result;
    }



    protected function upsert($sql, $param)
    {
        $result = $this->db->prepare($sql);
        $result->execute($param);
        return $this->db->lastInsertId();
    }

    protected function deleteValue($sql, $param = array())
    {
        $result = $this->db->prepare($sql);
        $result->execute($param);
    }
}