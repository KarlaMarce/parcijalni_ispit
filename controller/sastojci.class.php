<?php

namespace Controller;

class Sastojci extends Base
{
    public function findOne($id)
    {
        $sql = "SELECT * FROM sastojci WHERE id=:id";
        $param = array('id' => $id);

        $response = $this->select($sql, $param);

        return $response;
    }

    public function findAll()
    {
        $sql = "SELECT * FROM `sastojci`";
        $response = $this->select($sql);

        return array(
            'totalCount' => count($response),
            'data' => $response
        );
    }

    public function getNumberOfRows()
    {
        $sql = "SELECT COUNT(*) AS cnt FROM `sastojci`";
        $response = $this->select($sql);
        return $response;
    }

    public function save($param)
    {
        $sql = "INSERT INTO `sastojci` (`id`, `naziv`, `kolicina`, `mjerna_jedinica`) VALUES (:id, :naziv, :kolicina, :mjerna_jedinica)";

        $response = $this->upsert($sql, $param);

        return $response;
    }

    public function update($param)
    {
        $sql = "UPDATE sastojci SET kolicina=:kolicina WHERE id=:id";

        $response = $this->upsert($sql, $param);

        return $response;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM sastojci WHERE id=:id";
        $param = array('id' => $id);

        $this->deleteValue($sql, $param);

        return array(
            'error' => false,
            'notice' => 'success'
        );
    }

   
}