<?php

namespace Controller;

class Recept extends Base
{
    public function findOne($id)
    {
        $sql = "SELECT * FROM recept WHERE id=:id";
        $param = array('id' => $id);

        $response = $this->select($sql, $param);

        return $response;
    }

    public function findAll()
    {
        $sql = "SELECT * FROM `recept`";
        $response = $this->select($sql);

        return array(
            'totalCount' => count($response),
            'data' => $response
        );
    }

    public function getNumberOfRows()
    {
        $sql = "SELECT COUNT(*) AS cnt FROM `recept`";
        $response = $this->select($sql);
        return $response;
    }

    public function save($param)
    {
        $param1=array(
            'id' => $param['id'],
            'naziv' => $param['naziv'],
            'vrijeme_pripreme' => $param['vrijeme_pripreme']
        );

        $sql = "INSERT INTO recept(id, naziv, vrijeme_pripreme) VALUES (:id, :naziv, :vrijeme_pripreme)";

        $response = $this->upsert($sql, $param1);

        $i=1;

        while(isset($param['recept_id'.$i])){
            $param2=array(
                'recept_id' => $param['recept_id'.$i],
                'sastojak_id' => $param['sastojak_id'.$i],
                'kolicina' => $param['kolicina'.$i]
            );
    
            $sql = "INSERT INTO recept_sastojci(recept_id,sastojak_id,kolicina) VALUES (:recept_id,:sastojak_id,:kolicina)";
            $response = $this->upsert($sql, $param2);

            ++$i;
        }
        

        return $response;
    }

    public function update($param)
    {
        $sql = "UPDATE recept SET naziv=:naziv, vrijeme_pripreme=:vrijeme_pripreme WHERE id=:id";

        $response = $this->upsert($sql, $param);

        return $response;
    }

    public function delete($id)
    {
        $sql="DELETE FROM recept_sastojci WHERE recept_id=:id";
        $param = array('id' => $id);

        $this->deleteValue($sql, $param);


        $sql = "DELETE FROM recept WHERE id=:id";
        

        $this->deleteValue($sql, $param);

        return array(
            'error' => false,
            'notice' => 'success'
        );
    }

   
}