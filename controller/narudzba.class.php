<?php

namespace Controller;

use Error;

class Narudzba extends Base
{
    public function findOne($id)
    {
        $sql = "SELECT * FROM narudzba WHERE id=:id";
        $param = array('id' => $id);

        $response = $this->select($sql, $param);

        return $response;
    }

    public function findAll()
    {
        $sql = "SELECT * FROM `narudzba`";
        $response = $this->select($sql);

        return array(
            'totalCount' => count($response),
            'data' => $response
        );
    }

    public function getNumberOfRows()
    {
        $sql = "SELECT COUNT(*) AS cnt FROM `narudzba`";
        $response = $this->select($sql);
        return $response;
    }

    public function save($param)
    {
        $this->db->beginTransaction();
        try
        {
            $param1=array(
                'id' => $param['id'],
                'vrijeme_upita' => $param['vrijeme_upita']
            );
    
            $sql = "INSERT INTO narudzba(id, vrijeme_upita) VALUES (:id, :vrijeme_upita)";
    
            $response = $this->upsert($sql, $param1);
    
            $param2=array(
                'recept_id' => $param['recept_id'],
                'narudzba_id' => $param['narudzba_id'],
                'kolicina' => $param['kolicina']
            );
    
            $sql = "INSERT INTO recept_narudzba(recept_id,narudzba_id,kolicina) VALUES (:recept_id,:narudzba_id,:kolicina)";
    
            $response = $this->upsert($sql, $param2);
    
            $kolicinaNarudzba = $param['kolicina'];
    
            $recept_id = array(
                'recept_id' => $param['recept_id']);
    
            $sql = "SELECT sastojak_id, kolicina FROM recept_sastojci WHERE recept_id=:recept_id";
            
            $result = $this->select3($sql, $recept_id);
    
            while($row = $result->fetch(\PDO::FETCH_ASSOC))
            {
                $sastojak_id = $row['sastojak_id'];
                $kolicina_sastojka_u_receptu = $row['kolicina'];
    
                $sql="SELECT kolicina FROM sastojci WHERE id=:id";
                $kolicina_sastojka = $this->select2($sql, array('id' => $sastojak_id));
                $kolicina_sastojka=$kolicina_sastojka['kolicina'];
    
    
                $update=array(
                    'id' => $sastojak_id,
                    'kolicina' => $kolicina_sastojka - $kolicina_sastojka_u_receptu * $kolicinaNarudzba
                );

                if($update['kolicina']<0)
                {
                    throw new Error('Nedovoljna kolicina sastojka');
                }
    
                $sql="UPDATE sastojci SET kolicina=:kolicina WHERE id=:id";
                $response=$this->upsert($sql, $update);
            }
            $this->db->commit();
            return $response;
        }catch(\Throwable $th)
        {
            $this->db->rollBack();
            return 'neuspjeh';
        }
        

       
    }

    public function update($param)
    {
        $sql = "UPDATE narudzba SET vrijeme_upita=:vrijeme_upita WHERE id=:id";

        $response = $this->upsert($sql, $param);

        return $response;
    }

    public function delete($id)
    {
        $param = array('id' => $id);

        $sql = "DELETE FROM recept_narudzba WHERE narudzba_id=:id";
        $this->deleteValue($sql, $param);

        $sql = "DELETE FROM narudzba WHERE id=:id";
        

        $this->deleteValue($sql, $param);

        

        return array(
            'error' => false,
            'notice' => 'success'
        );
    }

   
}