<?php

namespace Lib;

class PdoMySql
{
    private $servername = "localhost";
    private $username = "root";
    private $password = "";
    private $database = "vjezba_3";

    public static $conn;

    public function __construct()
    {
        try
        {
            if (!isset(PdoMySql::$conn))
            {
                PdoMySql::$conn = new \PDO("mysql:host=$this->servername;dbname=$this->database", $this->username, $this->password);

                PdoMySql::$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            }
        }
        catch(\Throwable $th)
        {
            die($th->getMessage());
        }
    }

    public function getConn()
    {
        return PdoMySql::$conn;
    }
}