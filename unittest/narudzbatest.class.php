<?php

namespace Unittest;

class NarudzbaTest extends \PHPUnit\Framework\TestCase
{
    private $narudzba;

    public function __construct()
    {
        $this->narudzba = new \Controller\Narudzba;

        $this->findOne();
        $this->findAll();
        $this->save();
        $this->update();
        $this->delete();
    }

    public function __destruct()
    {
        $this->narudzba = null;
    }

    public function findOne()
    {
        $response = $this->narudzba->findOne(1);
        $this->assertEquals(1, count($response));
    }

    public function findAll()
    {
        $totalCount = $this->narudzba->getNumberOfRows();

        $this->assertEquals(1, count($totalCount));
        $this->assertNotEmpty($totalCount[0]);

        $response = $this->narudzba->findAll();
        $this->assertEquals($totalCount[0]['cnt'], count($response['data']));
    }

    public function save()
    {
        $param = array(
            'id' => 10,
            'vrijeme_upita' => '2024-03-15 13:15:00',
            'recept_id' => 6,
            'narudzba_id' => 10,
            'kolicina' => 3
        );

        $param1 = array(
            'id' => 10,
            'vrijeme_upita' => '2024-03-15 13:15:00'
            );

        $this->narudzba->save($param);
        $response = $this->narudzba->findOne($param['id']);
       
        $this->assertEquals($param1, $response[0]);

        $this->narudzba->delete($param['id']);
        $response = $this->narudzba->findOne($param['id']);
        $this->assertEmpty($response);
    }

    public function update()
    {
        $param = array(
            'id' => 10,
            'vrijeme_upita' => '2024-03-15 13:15:00',
            'recept_id' => 6,
            'narudzba_id' => 10,
            'kolicina' => 3
        );

        $new = array(
            'id' => 10,
            'vrijeme_upita' => '2024-03-15 13:15:00'
        );


        $this->narudzba->save($param);
        $response = $this->narudzba->findOne($param['id']);

        $this->assertEquals($new, $response[0]);

        $new = array(
            'id' => 10,
            'vrijeme_upita' => '2024-03-15 13:20:00'
        );

       

        $this->narudzba->update($new);
        $response = $this->narudzba->findOne($new['id']);


        $this->assertEquals($new, $response[0]);


        $this->narudzba->delete($new['id']);
        $response = $this->narudzba->findOne($new['id']);
        $this->assertEmpty($response);
    }

    public function delete()
    {
        $param = array(
            'id' => 10,
            'vrijeme_upita' => '2024-03-15 13:15:00',
            'recept_id' => 6,
            'narudzba_id' => 10,
            'kolicina' => 3
        );


        $this->narudzba->save($param);


        $expectedResult = array(
            'error' => false,
            'notice' => 'success'
        );

        $result = $this->narudzba->delete($param['id']);
        $this->assertEquals($expectedResult, $result);

    }
}