<?php

namespace Unittest;

class ReceptTest extends \PHPUnit\Framework\TestCase
{
    private $recept;

    public function __construct()
    {
        $this->recept = new \Controller\Recept;

        $this->findOne();
        $this->findAll();
        $this->save();
        $this->update();
        $this->delete();
    }

    public function __destruct()
    {
        $this->recept = null;
    }

    public function findOne()
    {
        $response = $this->recept->findOne(1);
        $this->assertEquals(1, count($response));
    }

    public function findAll()
    {
        $totalCount = $this->recept->getNumberOfRows();

        $this->assertEquals(1, count($totalCount));
        $this->assertNotEmpty($totalCount[0]);

        $response = $this->recept->findAll();
        $this->assertEquals($totalCount[0]['cnt'], count($response['data']));
    }

    public function save()
    {
        $param = array(
            'id' => 7,
            'naziv' => 'banana split',
            'vrijeme_pripreme' => 2,
            'recept_id1' => 7,
            'sastojak_id1' => 14,
            'kolicina1' => 1,
            'recept_id2' => 7,
            'sastojak_id2' => 15,
            'kolicina2' => 1,
            'recept_id3' => 7,
            'sastojak_id3' => 16,
            'kolicina3' => 1
        );

        $param1 = array(
            'id' => 7,
            'naziv' => 'banana split',
            'vrijeme_pripreme' => 2
        );


        $this->recept->save($param);
        $response = $this->recept->findOne($param['id']);
       


        $this->assertEquals($param1, $response[0]);

        $this->recept->delete($param['id']);
        $response = $this->recept->findOne($param['id']);
        $this->assertEmpty($response);
    }

    public function update()
    {
        $param = array(
            'id' => 7,
            'naziv' => 'banana split',
            'vrijeme_pripreme' => 2,
            'recept_id1' => 7,
            'sastojak_id1' => 14,
            'kolicina1' => 1,
            'recept_id2' => 7,
            'sastojak_id2' => 15,
            'kolicina2' => 1,
            'recept_id3' => 7,
            'sastojak_id3' => 16,
            'kolicina3' => 1
        );

        $new = array(
            'id' => 7,
            'naziv' => 'banana split',
            'vrijeme_pripreme' => 2
        );

        $this->recept->save($param);
        $response = $this->recept->findOne($param['id']);

        $this->assertEquals($new, $response[0]);


        $new = array(
            'id' => 7,
            'naziv' => 'banana split',
            'vrijeme_pripreme' => 3
        );

        $this->recept->update($new);
        $response = $this->recept->findOne($new['id']);


        $this->assertEquals($new, $response[0]);


        $this->recept->delete($new['id']);
        $response = $this->recept->findOne($new['id']);
        $this->assertEmpty($response);
    }

    public function delete()
    {
        $param = array(
            'id' => 7,
            'naziv' => 'banana split',
            'vrijeme_pripreme' => 2,
            'recept_id1' => 7,
            'sastojak_id1' => 14,
            'kolicina1' => 1,
            'recept_id2' => 7,
            'sastojak_id2' => 15,
            'kolicina2' => 1,
            'recept_id3' => 7,
            'sastojak_id3' => 16,
            'kolicina3' => 1
        );


        $this->recept->save($param);


        $expectedResult = array(
            'error' => false,
            'notice' => 'success'
        );

        $result = $this->recept->delete($param['id']);
        $this->assertEquals($expectedResult, $result);

    }
}