<?php

namespace Unittest;

class SastojciTest extends \PHPUnit\Framework\TestCase
{
    private $sastojak;

    public function __construct()
    {
        $this->sastojak = new \Controller\Sastojci;

        $this->findOne();
        $this->findAll();
        $this->save();
        $this->update();
        $this->delete();
    }

    public function __destruct()
    {
        $this->sastojak = null;
    }

    public function findOne()
    {
        $response = $this->sastojak->findOne(1);
        $this->assertEquals(1, count($response));
    }

    public function findAll()
    {
        $totalCount = $this->sastojak->getNumberOfRows();

        $this->assertEquals(1, count($totalCount));
        $this->assertNotEmpty($totalCount[0]);

        $response = $this->sastojak->findAll();
        $this->assertEquals($totalCount[0]['cnt'], count($response['data']));
    }

    public function save()
    {
        $param = array(
            'id' => 13,
            'naziv' => 'banana',
            'kolicina' => 50,
            'mjerna_jedinica' => 'kom'
        );

        $this->sastojak->save($param);
        $response = $this->sastojak->findOne($param['id']);

        $this->assertEquals($param, $response[0]);

        $this->sastojak->delete($param['id']);
        $response = $this->sastojak->findOne($param['id']);
        $this->assertEmpty($response);
    }

    public function update()
    {
        $param = array(
            'id' => 13,
            'naziv' => 'banana',
            'kolicina' => 50,
            'mjerna_jedinica' => 'kom'
        );


        $this->sastojak->save($param);
        $response = $this->sastojak->findOne($param['id']);

        $this->assertEquals($param, $response[0]);


        $new = array(
            'id' => 13,
            'kolicina' => 100,
        );

        $newValue = array(
            'id' => 13,
            'naziv' => 'banana',
            'kolicina' => 100,
            'mjerna_jedinica' => 'kom'
        );

        $this->sastojak->update($new);
        $response = $this->sastojak->findOne($new['id']);


        $this->assertEquals($newValue, $response[0]);


        $this->sastojak->delete($new['id']);
        $response = $this->sastojak->findOne($new['id']);
        $this->assertEmpty($response);
    }

    public function delete()
    {
        $param = array(
            'id' => 13,
            'naziv' => 'banana',
            'kolicina' => 50,
            'mjerna_jedinica' => 'kom'
        );


        $this->sastojak->save($param);


        $expectedResult = array(
            'error' => false,
            'notice' => 'success'
        );

        $result = $this->sastojak->delete($param['id']);
        $this->assertEquals($expectedResult, $result);

    }
}